#!/bin/bash -e

# inspired from https://github.com/eclipse/che-dockerfiles/blob/master/recipes/stack-base/centos/entrypoint.sh

if [ -n "$TRACE_ENTRYPOINT" ] ; then
  set -x
fi

export USER_ID=$(id -u)
export GROUP_ID=$(id -g)

if [ ${USER_ID} == 0 ] ; then
  export USER=root
  export GROUP=root
  export HOME=/root
else
  export USER=${USER:-lhcb}
  export GROUP=${GROUP:-lhcb}
  export HOME=/userhome

  if ! grep -Fq "${GROUP_ID}" /etc/group ; then
    # group id is unknown we have to add it
    if [ "${GROUP}" != lhcb ] ; then
      groupadd -g ${GROUP_ID} ${GROUP}
    else
      groupmod -g ${GROUP_ID} lhcb
    fi
  fi
  if ! grep -q "^[^:]*:[^:]*:${USER_ID}:" /etc/passwd ; then
    # user id is unknown we have to add it
    if [ "${USER}" != lhcb ] ; then
      useradd -u ${USER_ID} -g ${GROUP} -M -d ${HOME} -G root,wheel,users ${USER}
    else
      usermod -u ${USER_ID} -g ${GROUP} -m -d ${HOME} -G root,wheel,users lhcb
    fi
  fi
  if [ ! -e "${HOME}" ] ; then
    sudo mkdir -p ${HOME}
    sudo cp -a /etc/skel/. ${HOME}
  fi
  if [ $(stat --format=%u:%g ${HOME}) != ${USER_ID}:${GROUP_ID} ] ; then
    sudo chown -R ${USER_ID}:${GROUP_ID} ${HOME}
  fi
fi

if [ -z "$NO_CVMFS" -a -e /opt/cctools/bin/parrot_run ] && ! /opt/cctools/bin/parrot_run --is-running > /dev/null ; then
  if [ ! -e /cvmfs/lhcb.cern.ch/lib/LbLogin.sh ] ; then
    # no cvmfs, try using parrot
    # note: change directory to avoid parrot cvmfs module to pollute workspace
    workdir=$PWD
    cd /tmp
    # check if we can run anything in parrot
    /opt/cctools/bin/parrot_run -w $workdir true || (
      echo "ERROR: in-container mount of /cvmfs requires '--security-opt seccomp=unconfined' option to docker run" && \
        false)
    /opt/cctools/bin/parrot_run -w $workdir $0 "$@"
    exit
  fi
fi

if [ -z "$LHCB_ENV_MODE" ] ; then
  if [ -z "$NO_LBLOGIN" ] ; then
    LHCB_ENV_MODE=lblogin
  else
    LHCB_ENV_MODE=none
  fi
fi

if [ "${LHCB_ENV_MODE/-dev/}" = lblogin -a $(python -c 'import sys; print "%s.%s" % sys.version_info[:2]') = "2.4" ] ; then
  # LbLogin does not work with Python < 2.7 (SLC5 is Python 2.4)
  export PATH=/cvmfs/lhcb.cern.ch/lib/lcg/external/Python/2.7.3/x86_64-slc5-gcc43-opt/bin:$PATH
fi

if [ "$LHCB_ENV_MODE" = lbenv ] ; then
  LHCB_ENV_MODE=lbenv-stable
elif [ "$LHCB_ENV_MODE" = lbenv-dev ] ; then
  LHCB_ENV_MODE=lbenv-testing
fi

case "$LHCB_ENV_MODE" in
  none)
    env_script=
    ;;
  lbenv-*)
    env_script=/cvmfs/lhcb.cern.ch/lib/${LHCB_ENV_MODE/lbenv-/LbEnv-}.sh
    env_opts="--sh ${CMTCONFIG:+--platform=${CMTCONFIG}} ${env_opts}"
    ;;
  lblogin-dev)
    env_script=/cvmfs/lhcb.cern.ch/lib/LbLoginDev.sh
    env_opts="--no-userarea ${QUIET_LBLOGIN:+--silent} ${env_opts}"
    ;;
  lblogin|*)
    env_script=/cvmfs/lhcb.cern.ch/lib/LbLogin.sh
    env_opts="--no-userarea ${QUIET_LBLOGIN:+--silent} ${env_opts}"
    ;;
esac

if [ -n "$env_script" ] ; then
  if [ -e "$env_script" ] ; then
    . "$env_script" $env_opts
  else
    echo "WARNING: LHCb environment no avilable"
  fi
fi

exec "$@"
